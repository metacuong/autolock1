include(../globals.pri)

QT += core dbus
QT -= gui

CONFIG += console
CONFIG -= app_bundle
CONFIG += link_pkgconfig
CONFIG += qmsystem2

CONFIG += mobility
MOBILITY += sensors

TARGET = autolockd
TEMPLATE = app

SOURCES += \
    src/main.cpp \
    src/sensorshandler.cpp

contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/autolock/bin
    services.files = *.service
    services.path = /usr/share/dbus-1/services
    autorun.files = *.conf
    autorun.path = /etc/init/apps
    INSTALLS += target services autorun
}

OTHER_FILES += \
    com.cuonglb.autolockd.service

HEADERS += \
    src/sensorshandler.h

INCLUDEPATH += "../global"

DEFINES += CONSOLE_LOGS_MODE
