/*
 * Auto Lock for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "sensorshandler.h"

SensorsHandler::SensorsHandler(QObject *parent) :
    QObject(parent),
    qProximitySensor(new QProximitySensor(this)),
    qAmbientLightSensor(new QAmbientLightSensor(this)),
    m_isClosed(false),
    m_lightValReal(QAmbientLightReading::Undefined),
    m_timer(new QTimer(this))
{
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "Register object and service to session bus";
#endif
    QDBusConnection::sessionBus().registerObject(AL_OBJECT, this, QDBusConnection::ExportAllSlots | QDBusConnection::ExportAllSignals);
    QDBusConnection::sessionBus().registerService(AL_SERVICE);

    proximitySensorSetup();
    ambientLightSensorSetup();

    handlerSetup();

    lightValueChanged();
    intervalValueChanged();
}

SensorsHandler::~SensorsHandler() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "Unregister object and service to session bus";
#endif
    delete qProximitySensor;
    delete qAmbientLightSensor;
    QDBusConnection::sessionBus().unregisterObject(AL_OBJECT);
    QDBusConnection::sessionBus().unregisterService(AL_SERVICE);
}

void SensorsHandler::daemonState(const bool &state) {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << QString("daemonState = %1").arg(state?"Open":"Close");
#endif
    if (!state) {
        qProximitySensor->stop();
        qAmbientLightSensor->stop();
        QApplication::exit(0);
    }
}

void SensorsHandler::proximitySensorSetup() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "proximitySensorSetup()";
#endif
    QObject::connect(qProximitySensor, SIGNAL(readingChanged()), this, SLOT(proximitySensorChanged()));
    qProximitySensor->start();
}

void SensorsHandler::ambientLightSensorSetup() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "ambientLightSensorSetup()";
#endif
    m_lightVal = QAmbientLightReading::Dark;
    QObject::connect(qAmbientLightSensor, SIGNAL(readingChanged()), this, SLOT(ambientLightSensorChanged()));
    qAmbientLightSensor->start();
}

void SensorsHandler::proximitySensorChanged() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "proximitySensorChanged()";
#endif
    m_isClosed = qProximitySensor->reading()->close();
    if (m_isClosed)
        emit proximitySensorClose();
    else
        emit proximitySensorDistant();
}

void SensorsHandler::lightValueChanged() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "lightValueChanged()";
#endif
    m_lightVal = m_settings.getLightValue();
}


void SensorsHandler::ambientLightSensorChanged() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "ambientLightSensorChanged()";
#endif
    if (qAmbientLightSensor->reading()->lightLevel() != QAmbientLightReading::Undefined) {
        /*! It should provide an option to user that can set the default value for Ambient Light Sensor */
        m_lightValReal = qAmbientLightSensor->reading()->lightLevel();
        emit ambientLightSensorLightChanged();
    }
}

void SensorsHandler::maybeLockScreen() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "maybeLockScreen()";
    qDebug() << QString("* Proximity Sensor state = %1").arg(m_isClosed?"Open":"Close");
    qDebug() << QString("* Ambient Light Sensor value = %1").arg(QString::number(m_lightVal));
#endif
    if (m_lightVal >= m_lightValReal && m_isClosed && !m_is_waiting_to_lock_screen) {
        if (!m_interval)
            lock_screen();
        else  {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << QString("Lock screen will be activated after x secs ").arg(m_interval);
#endif
            m_is_waiting_to_lock_screen = true;
            m_timer->start(m_interval*1000);// lock screen after x seconds
        }
    }
}

void SensorsHandler::maybeCancelLockScreen() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "maybeCancelLockScreen()";
    qDebug() << QString("* Proximity Sensor state = %1").arg(m_isClosed?"Open":"Close");
    qDebug() << QString("* Ambient Light Sensor value = %1").arg(QString::number(m_lightVal));
#endif
    if (m_interval && !m_isClosed && m_is_waiting_to_lock_screen) {
        if (m_timer->isActive()) {
#ifdef CONSOLE_LOGS_MODE
  qDebug() << QString("Lock screen will be cancelled");
#endif
            m_lock_screen_cancelled = true;
        }
    }
}

void SensorsHandler::handlerSetup() {
#ifdef CONSOLE_LOGS_MODE
    qDebug() << "handlerSetup()";
#endif
    m_is_waiting_to_lock_screen = false;
    m_lock_screen_cancelled = false;
    m_timer->setSingleShot(true);
    QObject::connect(m_timer, SIGNAL(timeout()), this, SLOT(lock_screen()));
    QObject::connect(this, SIGNAL(proximitySensorClose()), SLOT(maybeLockScreen()));
    QObject::connect(this, SIGNAL(ambientLightSensorLightChanged()), SLOT(maybeLockScreen()));
    QObject::connect(this, SIGNAL(proximitySensorDistant()), SLOT(maybeCancelLockScreen()));
}

void SensorsHandler::intervalValueChanged() {
    m_interval = m_settings.getIntervalValue();
}

void SensorsHandler::lock_screen() {
    if (!m_lock_screen_cancelled)
        qmdisplayState.set(MeeGo::QmDisplayState::Off);
    m_isClosed = false;
    m_is_waiting_to_lock_screen = false;
    m_lock_screen_cancelled = false;
}
