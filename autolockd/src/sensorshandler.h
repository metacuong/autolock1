/*
 * Auto Lock for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SENSORSHANDLER_H
#define SENSORSHANDLER_H

#include <QTimer>
#include <QObject>
#include <QDBusConnection>
#include <QtGui/QApplication>

#ifdef CONSOLE_LOGS_MODE
#include <QDebug>
#endif

#include <qmdisplaystate.h>

#include <QProximitySensor>
#include <QAmbientLightSensor>

#include "global.h"
#include "settings.h"

QTM_USE_NAMESPACE

class SensorsHandler : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.cuonglb.autolockd")
public:
    explicit SensorsHandler(QObject *parent = 0);
    ~SensorsHandler();
    
signals:
    void proximitySensorClose();
    void proximitySensorDistant();
    void ambientLightSensorLightChanged();

public slots:
    /*! if state = true then start daemon else kill me */
    Q_NOREPLY void daemonState(const bool&);
    Q_NOREPLY void lightValueChanged();
    Q_NOREPLY void intervalValueChanged();

private slots:
    void proximitySensorChanged();
    void ambientLightSensorChanged();
    void maybeLockScreen();
    void maybeCancelLockScreen();
    void lock_screen();


private:
    void proximitySensorSetup();
    void ambientLightSensorSetup();
    void handlerSetup();

    QProximitySensor *qProximitySensor;
    QAmbientLightSensor *qAmbientLightSensor;
    MeeGo::QmDisplayState qmdisplayState;

    int m_lightVal;
    bool m_isClosed;
    int m_lightValReal;
    Settings m_settings;
    int m_interval;
    bool m_is_waiting_to_lock_screen;

    QTimer *m_timer;
    bool m_lock_screen_cancelled;

};

#endif // SENSORSHANDLER_H
