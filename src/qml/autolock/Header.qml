import QtQuick 1.1

Item {
    width: parent.width
    height: 70

    z : 1

    property alias title: h_title.text
    property alias text_bold: h_title.font.bold

    Rectangle {
        width: parent.width
        height: parent.height
        color:"#f2f2f2"
    }

    Rectangle {
        id: top_banner
        x: -1
        y: -1
        width: parent.width + 1
        height: 71
        color: "#395994"

        Text {
            id:h_title
            anchors.verticalCenter: parent.verticalCenter
            x:20
            color: "#ffffff"
            font.family: "Nokia Pure Text Bold"
            font.pixelSize: 35
        }
    }

    Rectangle {
        id: bottom_line
        x: -1
        y: top_banner.height -1
        width: parent.width + 1
        height: 1
        color: "whitesmoke"
    }
}
