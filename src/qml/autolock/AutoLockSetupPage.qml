import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    orientationLock: PageOrientation.LockPortrait

    Header {id: headerPage; title: qsTr("Auto Lock Screen Setup")}

    Flickable {
        id: fickableMain

        anchors.top: headerPage.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        contentHeight: cols.height + 2*16

        clip: true
        flickableDirection: Flickable.VerticalFlick

        Column {
            id: cols

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 16
            anchors.rightMargin: 16

            spacing: 16

            Rectangle {
                id: item
                height: enabledTitleLabel.height + enabledDescriptionLabel.height + 16
                width: parent.width
                color: "transparent"
                z: 2

                Label {
                    id: enabledTitleLabel
                    anchors.top: parent.top
                    anchors.topMargin: 16
                    anchors.left: parent.left
                    anchors.right: enabledSwitch.left
                    text: "Activate auto lock screen"
                }

                Label {
                    id: enabledDescriptionLabel
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: enabledSwitch.left
                    color: "#808080"
                    wrapMode: Text.WordWrap
                    text: "Enable to use proximity sensor to lock screen"
                }

                Switch {
                    id: enabledSwitch
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    checked: Settings.getDaemonState()
                    onCheckedChanged: {
                        Settings.setDaemonState(checked)
                        ClientInterface.setDaemonState(checked)
                        timerItem.enabled = checked
                        ambientItem.enabled = checked
                    }
                }
            }

            Column {
                width: parent.width
                spacing: 16

                GroupItem {title: qsTr("Ambient Light Sensor setup")}

                Item {
                    id: ambientItem
                    width: parent.width
                    height: aTitle.height + aDesc.height
                    enabled: Settings.getDaemonState()

                    Rectangle {
                        id: aSelect
                        width: parent.width
                        height: aTitle.height + aDesc.height
                        color: "transparent"
                        Label {
                            id: aTitle
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.right: parent.right
                            color: ambientItem.enabled?"white":"grey"
                            text: __aTitleTextChanged()
                        }

                        Label {
                            id: aDesc
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.right: parent.right
                            color: "#808080"
                            wrapMode: Text.WordWrap
                            width: parent.width - 16
                            text: qsTr("Define light level value to use it with ambient light sensor.")
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: aSelectMenu.open()
                            onPressed: aSelect.color = "#5F6B70"
                            onCanceled: aSelect.color = "transparent"
                            onReleased: aSelect.color = "transparent"
                        }

                    }

                }

                GroupItem {title: qsTr("Lock screen timer")}

                Item {
                    id: timerItem
                    width: parent.width
                    height: tTitle.height + tDesc.height
                    enabled: Settings.getDaemonState()

                    Rectangle {
                        id: tSelect
                        width: parent.width
                        height: tTitle.height + tDesc.height
                        color: "transparent"
                        Label {
                            id: tTitle
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.right: parent.right
                            color: timerItem.enabled?"white":"grey"
                            text: __tTitleTextChanged()
                        }

                        Label {
                            id: tDesc
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.right: parent.right
                            color: "#808080"
                            wrapMode: Text.WordWrap
                            width: parent.width - 16
                            text: qsTr("Interval in seconds set the time interval to active lock screen. The lock screen will be activated automatically after that much seconds. You can set it from 0 seconds to 5 seconds.")
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: tSelectMenu.open()
                            onPressed: tSelect.color = "#5F6B70"
                            onCanceled: tSelect.color = "transparent"
                            onReleased: tSelect.color = "transparent"
                        }

                    }

                }

            }

        }
    }

    function __lightLevel(intLevel) {
        var strLevel = qsTr("Drak")
        switch(intLevel) {
        case 2: strLevel = qsTr("Twilight"); break
        case 3: strLevel = qsTr("Light"); break
        case 4: strLevel = qsTr("Bright"); break
        case 5: strLevel = qsTr("Sunny"); break
        }
        return strLevel
    }


    function __lightLevelSet(intLevel) {
        Settings.setLightValue(intLevel)
        ClientInterface.lightValueChanged()
        __aTitleTextChanged()
    }

    function __tMenuItem(intSecs) {
        var strSecs = qsTr("After %1 secs").arg(intSecs)
        if (intSecs === 0)
            strSecs = strSecs + qsTr(" (immediately)")
        return strSecs
    }

    function __intervalSet(intSecs) {
        Settings.setIntervalValue(intSecs)
        ClientInterface.intervalValueChanged()
        __tTitleTextChanged()
    }

    function __aTitleTextChanged() {
        aTitle.text = qsTr("Light level: <b>%1</b>").arg(__lightLevel(Settings.getLightValue()))
    }

    function __tTitleTextChanged() {
        var __intervalVal = Settings.getIntervalValue()
        tTitle.text = qsTr("Lock screen after <b>%1 secs%2</b>").arg(__intervalVal).arg(__intervalVal === 0?qsTr(" (immediately)"):"")
    }

    ContextMenu {
        id: aSelectMenu
        MenuLayout {
            MenuItem { text: __lightLevel(1); onClicked:__lightLevelSet(1)}
            MenuItem { text: __lightLevel(2); onClicked: __lightLevelSet(2)}
            MenuItem { text: __lightLevel(3); onClicked: __lightLevelSet(3)}
            MenuItem { text: __lightLevel(4); onClicked: __lightLevelSet(4)}
            MenuItem { text: __lightLevel(5); onClicked: __lightLevelSet(5)}
        }
    }

    ContextMenu {
        id: tSelectMenu
        MenuLayout {
            MenuItem { text: __tMenuItem(0); onClicked: __intervalSet(0)}
            MenuItem { text: __tMenuItem(1); onClicked: __intervalSet(1)}
            MenuItem { text: __tMenuItem(2); onClicked: __intervalSet(2)}
            MenuItem { text: __tMenuItem(3); onClicked: __intervalSet(3)}
            MenuItem { text: __tMenuItem(4); onClicked: __intervalSet(4)}
            MenuItem { text: __tMenuItem(5); onClicked: __intervalSet(5)}
        }
    }
}
