import QtQuick 1.1
import com.nokia.meego 1.0
import QtMobility.sensors 1.2

Page {
    tools: commonTools
    orientationLock: PageOrientation.LockPortrait

    property bool proximityValue: false
    property int ambientLight: -1
    property int rectHeight: 51

    Header {title: qsTr("Sensors Test")}

    ProximitySensor {
        id: proximitySensor
        active: true
        onReadingChanged: proximityValue = reading.close
    }

    AmbientLightSensor {
        id: ambientSensor
        active: true
        onReadingChanged: ambientLight = reading.lightLevel
    }

    Item {
        id: rectContainer
        width: parent.width
        height: 490

        Image {
            id: proximityImage
            x: 120
            y: 124
            source: proximityValue ? "icons/proximity_close.png" : "icons/proximity_distant.png"
            anchors.centerIn: parent
            width: proximityValue ? 200 : 120
            height: proximityValue ? 200 : 120
            anchors.verticalCenterOffset: 34
            anchors.horizontalCenterOffset: 0
            smooth: true

            Label {
                id: proximityText
                anchors.centerIn: parent
                text: proximityValue ? qsTr("Close") : qsTr("Distant")
            }
        }
    }

    Item {
        id: ambientColumn
        width: parent.width
        anchors.top: rectContainer.bottom
        height: 300

        Rectangle {
            id: topSeparator
            width:  parent.width; height: 1
            color: "#EA650A"
        }

        Rectangle {
            id: sunnyRect
            x: 0
            y: 1
            width:  parent.width; height: rectHeight
            color: ambientLight === 5 ? "#F7F2F4" : "black"

            Label {
                text: "Sunny"
                color: "black"
                visible: ambientLight === 5 ? true : false
                anchors { fill: parent; margins: 10 }
            }
        }

        Rectangle {
            id: sunnySeparator
            width:  parent.width; height: 1
            color: "#F7F2F4"
        }

        Rectangle {
            id: brightRect
            x: 0
            y: 52
            width:  parent.width
            height: rectHeight
            color: ambientLight >= 4 ? "#E0C5D0" : "black"

            Label {
                text: "Bright"
                color: "black"
                visible: ambientLight  === 4 ? true : false
                anchors { fill: parent; margins: 10 }
            }
        }

        Rectangle {
            id: brightSeparator
            width:  parent.width; height: 1
            color: "#E0C5D0"
        }

        Rectangle {
            id: lightRect
            x: 0
            y: 103
            width:  parent.width; height: rectHeight
            color: ambientLight >= 3 ? "#C99FB0" : "black"

            Label {
                text: ambientLight < 1 ? "Undefined" : "Light"
                color: "black"
                anchors { fill: parent; margins: 10 }
                visible: ( ambientLight === 3 || ambientLight < 1 ) ? true : false
            }
        }

        Rectangle {
            id: lightSeparator
            width:  parent.width; height: 1
            color: "#C99FB0"
        }

        Rectangle {
            id: twilightRect
            x: 0
            y: 154
            width:  parent.width; height: rectHeight
            color: ambientLight >= 2 ? "#AB798D" : "black"

            Label {
                text: "Twilight"
                color: "black"
                visible: ambientLight === 2 ? true : false
                anchors { fill: parent; margins: 10 }
            }
        }

        Rectangle {
            id: twilightSeparator
            width:  parent.width; height: 1
            color: "#AB798D"
        }

        Rectangle {
            id: darkRect
            x: 0
            y: 205
            width:  parent.width; height: rectHeight
            color: ambientLight >= 1 ? "#945770" : "black"

            Label {
                text: "Dark"
                color: "black"
                visible: ambientLight === 1 ? true : false
                anchors { fill: parent; margins: 10 }
            }
        }

    }

    Label {
        id: text1
        x: 12
        y: 174
        width: 336
        height: 28
        text: qsTr("Proximity Sensor")
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 22
    }

    Label {
        id: text2
        x: 12
        y: 444
        width: 336
        height: 28
        text: qsTr("Ambient Light Sensor")
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 22
        horizontalAlignment: Text.AlignHCenter
    }

    states: [ State {
            name: "close"; when: proximityValue
            PropertyChanges {
                target: proximityImage
                width: 120
                height: 120
                source: "icons/proximity_close.png"
            }
        },

        State {
            name: "distant"; when: !proximityValue
            PropertyChanges {
                target: proximityImage
                width: 120
                height: 120
                source: "icons/proximity_distant.png"
            }
        }
    ]

    transitions: Transition {
        NumberAnimation { properties: "width,height,source"; duration: 100 }
    }
}
