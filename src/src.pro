include(../globals.pri)

CONFIG += qdeclarative-boostable
CONFIG += mobility

MOBILITY += feedback

QT += core dbus declarative

TARGET = autolock
TEMPLATE = app

SOURCES += main.cpp \
    clientinterface.cpp

contains(MEEGO_EDITION,harmattan) {
    target.path = /opt/autolock/bin
    #qml.files = qml/autolock
    #qml.path = /opt/autolock/qml
    desktop.files = autolock.desktop
    desktop.path = /usr/share/applications
    icon.files = ../autolock.png
    icon.path = /usr/share/icons/hicolor/80x80/apps
    splash.files = ../splash.png
    splash.path = /opt/autolock
    INSTALLS += target desktop icon style splash #qml
}

INCLUDEPATH += "../global"

HEADERS += \
    clientinterface.h

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    qml/autolock/About.qml \
    qml/autolock/AutoLockSetupPage.qml \
    qml/autolock/Header.qml \
    qml/autolock/main.qml \
    qml/autolock/SensorsTestPage.qml \
    qml/autolock/GroupItem.qml
