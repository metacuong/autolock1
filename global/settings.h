/*
 * Auto Lock for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QStringList>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);
    
signals:
    
public slots:
    Q_INVOKABLE void setDaemonState(const bool&);
    Q_INVOKABLE bool getDaemonState() const;

    Q_INVOKABLE void setLightValue(const int&);
    Q_INVOKABLE int getLightValue() const;

    Q_INVOKABLE void setIntervalValue(const int&);
    Q_INVOKABLE int getIntervalValue() const;

private:
    QVariant sGet(QString, QString, const QVariant&) const;
    void sStore(QString, QString, QVariant);
    
};

#endif // SETTINGS_H
