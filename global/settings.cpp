/*
 * Auto Lock for MeeGo 1.2 Harmattan
 * Copyright (C) 2013 Cuong Le <metacuong@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "settings.h"

#define DEFAULT_LIGHT_VALUE 1 //Drak
#define DEFAULT_DAEMON_STARTED false //not started yet
#define DEFAULT_INTERVAL_VALUE 0 //immediately

Settings::Settings(QObject *parent) :
    QObject(parent)
{
}

void Settings::setDaemonState(const bool &val) {
    sStore("daemon", "started", val);
}

bool Settings::getDaemonState() const {
    return sGet("daemon", "started", DEFAULT_DAEMON_STARTED).toBool();
}

void Settings::setLightValue(const int &val) {
    sStore("ambient_light_sensor", "lightvalue", val);
}

int Settings::getLightValue() const {
    return sGet("ambient_light_sensor", "lightvalue", DEFAULT_LIGHT_VALUE).toInt();
}

void Settings::setIntervalValue(const int &val) {
    sStore("timer", "interval", val);
}

int Settings::getIntervalValue() const {
    return sGet("timer", "interval", DEFAULT_INTERVAL_VALUE).toInt();
}


QVariant Settings::sGet(QString groupName, QString keyName, const QVariant &defaultVal) const {
    QSettings settings;
    settings.beginGroup(groupName);
    if (settings.childKeys().indexOf(keyName) == -1)
        return defaultVal;
    return settings.value(keyName);
}

void Settings::sStore(QString groupName, QString keyName, QVariant val) {
    QSettings settings;
    settings.setValue(QString("%1/%2").arg(groupName).arg(keyName), val);
}
