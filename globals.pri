VERSION_MAJOR = 1	
VERSION_MINOR = 0
VERSION_PATCH = 1

DEFINES += 'APPVERSION_MAJOR=$${VERSION_MAJOR}'
DEFINES += 'APPVERSION_MINOR=$${VERSION_MINOR}'
DEFINES += 'APPVERSION_PATCH=$${VERSION_PATCH}'

DEFINES += ORGANIZATION_NAME=\"\\\"cuonglb\\\"\"
DEFINES += APPLICATION_NAME=\"\\\"AutoLock\\\"\"

HEADERS += \
    ../global/settings.h

SOURCES += \
    ../global/settings.cpp
